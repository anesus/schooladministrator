<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	
	<title>Tuition payment</title>
		
<link rel='stylesheet' type='text/css' href='/vendor/css/style.css' />
	<link rel='stylesheet' type='text/css' href='css/print.css' media="print" />
	<script type='text/javascript' src='/vendor/js/jquery-1.3.2.min.js'></script>
	<script type='text/javascript' src='/vendor/js/example.js'></script>

</head>

<body>
<%@page import="com.stewardbank.api.Main.Payment"%>

<%Payment p=(Payment)session.getAttribute("payment");%>
	<div id="page-wrap">

		 <label  id="header"></label> 
		
		<div id="identity">

            <div id="logo" >
 
       
              <img id="image" src="images/logo2.png" alt="logo"  />
            </div>
		
		</div>
		
		<br />
			<br />
			<br />
		
		
		<div style="clear:both"></div>
		
		<div id="customer">

          

            <table id="meta">
                <tr>
                    <td class="meta-head"></td>
                    <td><label> <% out.println(p.getReference()); %></label></td>
                </tr>
                <tr>

                    <td class="meta-head">Date</td>
                    <td><textarea readonly id="date"></textarea></td>
                </tr>
                <tr>
                    <td class="meta-head"></td>
                    <td><div class="due"></div></td>
                </tr>

            </table>
		
		</div>
		
		<br />
			<br />
			<br />
		
		
		
		<table id="items">
		
		  <tr>
		      <th><% out.println(p.getBeneficiary()); %></th>
		      <th>Registration ID</th>
		      <th>Sum paid</th>
		      <th>Paid by</th>
		      <th>Approved</th>
		  </tr>
		  
		  <tr class="item-row">
		      <!-- <td class="item-name"><div class="delete-wpr"><label>Web Updates</label><a class="delete" href="javascript:;" title="Remove row">X</a></div></td> -->
		      <td class="name"><label>(<strong><% out.println(p.getBeneficiary()); %></strong>here)</label></td>
		      <td><label class="reg"><% out.println(p.getReference()); %></label></td>
		      <td><label class="amount"><% out.println(p.getAmount()); %></label></td>
		      <td><span class="mobile"><% out.println(p.getPhone()); %></span></td>
			  <td><span class="proved">Yes</span></td>
		  </tr>
		
		  <tr>

		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Amount Paid</td>
		      <td class="total-value"><div id="total"><% out.println(p.getAmount()); %></div></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line">Total</td>

		      <td class="total-value"><textarea id="paid"><% out.println(p.getAmount()); %></textarea></td>
		  </tr>
		  <tr>
		      <td colspan="2" class="blank"> </td>
		      <td colspan="2" class="total-line balance"></td>
		      <td class="total-value balance"><div class="due"></div></td>
		  </tr>
		
		</table>
		
		<div id="terms">
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
			<br>
		  <h5>Powered by <strong>Steward Bank</strong>&#153</h5>
		  <!-- <textarea readonly></textarea> -->
		</div>
	
	


         <button id="cmd" onclick="document.getElementById('cmd').style.visibility='hidden';window.print();return true;action();" style= font-family:><a href="/dashy">Print/Download as pdf</a>
		<script>
		
						</script>
		 </button>
		 
         <div> <p> </p> </div>


	
</body>

</html>