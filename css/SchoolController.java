package com.stewardbank.api.Main;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SchoolController {
	
	@Autowired
	SchoolService schoolService;

	  @RequestMapping(value = "/secured/school/signup", method = RequestMethod.POST,
	            consumes = MediaType.APPLICATION_JSON_VALUE,
	            produces = MediaType.APPLICATION_JSON_VALUE)
	    /*    @Secured("ROLE_USER")
		    @PreAuthorize("hasRole('ROLE_USER')")*/
	    public StewardBankApiResponse<?> authZesa(@RequestBody Registration registration) throws Exception {

	        StewardBankApiResponse<?> response =   schoolService.processRegistration(registration);
	        return response;

	    }
	  

	  
	  @RequestMapping(value = "/secured/school/activate", method = RequestMethod.POST,
	            consumes = MediaType.APPLICATION_JSON_VALUE,
	            produces = MediaType.APPLICATION_JSON_VALUE)
	    /*    @Secured("ROLE_USER")
		    @PreAuthorize("hasRole('ROLE_USER')")*/
	    public StewardBankApiResponse<?> activateAcc(@RequestBody Activation  activation) throws Exception {

	        StewardBankApiResponse<?> response =   schoolService.processLogin(activation);
	        return response;

	    }
	  

}
